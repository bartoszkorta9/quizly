package com.quizly.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.quizly.IntegrationTest;
import com.quizly.domain.DragQuestion;
import com.quizly.repository.DragQuestionRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link DragQuestionResource} REST controller.
 */

@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class DragQuestionResourceIT {
    //
    //    private static final String DEFAULT_ANSWER_ONE = "AAAAAAAAAA";
    //    private static final String UPDATED_ANSWER_ONE = "BBBBBBBBBB";
    //
    //    private static final String DEFAULT_ANSWER_TWO = "AAAAAAAAAA";
    //    private static final String UPDATED_ANSWER_TWO = "BBBBBBBBBB";
    //
    //    private static final String DEFAULT_ANSWER_THREE = "AAAAAAAAAA";
    //    private static final String UPDATED_ANSWER_THREE = "BBBBBBBBBB";
    //
    //    private static final String DEFAULT_ANSWER_FOUR = "AAAAAAAAAA";
    //    private static final String UPDATED_ANSWER_FOUR = "BBBBBBBBBB";
    //
    //    private static final String DEFAULT_CORRECT_ORDER = "AAAAAAAAAA";
    //    private static final String UPDATED_CORRECT_ORDER = "BBBBBBBBBB";
    //
    //    private static final String ENTITY_API_URL = "/api/drag-questions";
    //    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    //
    //    private static Random random = new Random();
    //    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    //
    //    @Autowired
    //    private DragQuestionRepository dragQuestionRepository;
    //
    //    @Autowired
    //    private EntityManager em;
    //
    //    @Autowired
    //    private MockMvc restDragQuestionMockMvc;
    //
    //    private DragQuestion dragQuestion;
    //
    //    /**
    //     * Create an entity for this test.
    //     *
    //     * This is a static method, as tests for other entities might also need it,
    //     * if they test an entity which requires the current entity.
    //     */
    //
    //    public static DragQuestion createEntity(EntityManager em) {
    //        DragQuestion dragQuestion = new DragQuestion()
    //            .answerOne(DEFAULT_ANSWER_ONE)
    //            .answerTwo(DEFAULT_ANSWER_TWO)
    //            .answerThree(DEFAULT_ANSWER_THREE)
    //            .answerFour(DEFAULT_ANSWER_FOUR)
    //            .correctOrder(DEFAULT_CORRECT_ORDER);
    //        return dragQuestion;
    //    }
    //    /*
    //    /**
    //     * Create an updated entity for this test.
    //     *
    //     * This is a static method, as tests for other entities might also need it,
    //     * if they test an entity which requires the current entity.
    //     */
    //    public static DragQuestion createUpdatedEntity(EntityManager em) {
    //        DragQuestion dragQuestion = new DragQuestion()
    //            .answerOne(UPDATED_ANSWER_ONE)
    //            .answerTwo(UPDATED_ANSWER_TWO)
    //            .answerThree(UPDATED_ANSWER_THREE)
    //            .answerFour(UPDATED_ANSWER_FOUR)
    //            .correctOrder(UPDATED_CORRECT_ORDER);
    //        return dragQuestion;
    //    }
    //
    //    @BeforeEach
    //    public void initTest() {
    //        dragQuestion = createEntity(em);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void createDragQuestion() throws Exception {
    //        int databaseSizeBeforeCreate = dragQuestionRepository.findAll().size();
    //        // Create the DragQuestion
    //        restDragQuestionMockMvc
    //            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dragQuestion)))
    //            .andExpect(status().isCreated());
    //
    //        // Validate the DragQuestion in the database
    //        List<DragQuestion> dragQuestionList = dragQuestionRepository.findAll();
    //        assertThat(dragQuestionList).hasSize(databaseSizeBeforeCreate + 1);
    //        DragQuestion testDragQuestion = dragQuestionList.get(dragQuestionList.size() - 1);
    //        assertThat(testDragQuestion.getAnswerOne()).isEqualTo(DEFAULT_ANSWER_ONE);
    //        assertThat(testDragQuestion.getAnswerTwo()).isEqualTo(DEFAULT_ANSWER_TWO);
    //        assertThat(testDragQuestion.getAnswerThree()).isEqualTo(DEFAULT_ANSWER_THREE);
    //        assertThat(testDragQuestion.getAnswerFour()).isEqualTo(DEFAULT_ANSWER_FOUR);
    //        assertThat(testDragQuestion.getCorrectOrder()).isEqualTo(DEFAULT_CORRECT_ORDER);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void createDragQuestionWithExistingId() throws Exception {
    //        // Create the DragQuestion with an existing ID
    //        dragQuestion.setId(1L);
    //
    //        int databaseSizeBeforeCreate = dragQuestionRepository.findAll().size();
    //
    //        // An entity with an existing ID cannot be created, so this API call must fail
    //        restDragQuestionMockMvc
    //            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dragQuestion)))
    //            .andExpect(status().isBadRequest());
    //
    //        // Validate the DragQuestion in the database
    //        List<DragQuestion> dragQuestionList = dragQuestionRepository.findAll();
    //        assertThat(dragQuestionList).hasSize(databaseSizeBeforeCreate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void checkAnswerOneIsRequired() throws Exception {
    //        int databaseSizeBeforeTest = dragQuestionRepository.findAll().size();
    //        // set the field null
    //        dragQuestion.setAnswerOne(null);
    //
    //        // Create the DragQuestion, which fails.
    //
    //        restDragQuestionMockMvc
    //            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dragQuestion)))
    //            .andExpect(status().isBadRequest());
    //
    //        List<DragQuestion> dragQuestionList = dragQuestionRepository.findAll();
    //        assertThat(dragQuestionList).hasSize(databaseSizeBeforeTest);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void checkAnswerTwoIsRequired() throws Exception {
    //        int databaseSizeBeforeTest = dragQuestionRepository.findAll().size();
    //        // set the field null
    //        dragQuestion.setAnswerTwo(null);
    //
    //        // Create the DragQuestion, which fails.
    //
    //        restDragQuestionMockMvc
    //            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dragQuestion)))
    //            .andExpect(status().isBadRequest());
    //
    //        List<DragQuestion> dragQuestionList = dragQuestionRepository.findAll();
    //        assertThat(dragQuestionList).hasSize(databaseSizeBeforeTest);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void checkCorrectOrderIsRequired() throws Exception {
    //        int databaseSizeBeforeTest = dragQuestionRepository.findAll().size();
    //        // set the field null
    //        dragQuestion.setCorrectOrder(null);
    //
    //        // Create the DragQuestion, which fails.
    //
    //        restDragQuestionMockMvc
    //            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dragQuestion)))
    //            .andExpect(status().isBadRequest());
    //
    //        List<DragQuestion> dragQuestionList = dragQuestionRepository.findAll();
    //        assertThat(dragQuestionList).hasSize(databaseSizeBeforeTest);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void getAllDragQuestions() throws Exception {
    //        // Initialize the database
    //        dragQuestionRepository.saveAndFlush(dragQuestion);
    //
    //        // Get all the dragQuestionList
    //        restDragQuestionMockMvc
    //            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
    //            .andExpect(status().isOk())
    //            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
    //            .andExpect(jsonPath("$.[*].id").value(hasItem(dragQuestion.getId().intValue())))
    //            .andExpect(jsonPath("$.[*].answerOne").value(hasItem(DEFAULT_ANSWER_ONE)))
    //            .andExpect(jsonPath("$.[*].answerTwo").value(hasItem(DEFAULT_ANSWER_TWO)))
    //            .andExpect(jsonPath("$.[*].answerThree").value(hasItem(DEFAULT_ANSWER_THREE)))
    //            .andExpect(jsonPath("$.[*].answerFour").value(hasItem(DEFAULT_ANSWER_FOUR)))
    //            .andExpect(jsonPath("$.[*].correctOrder").value(hasItem(DEFAULT_CORRECT_ORDER)));
    //    }
    //
    //    @Test
    //    @Transactional
    //    void getDragQuestion() throws Exception {
    //        // Initialize the database
    //        dragQuestionRepository.saveAndFlush(dragQuestion);
    //
    //        // Get the dragQuestion
    //        restDragQuestionMockMvc
    //            .perform(get(ENTITY_API_URL_ID, dragQuestion.getId()))
    //            .andExpect(status().isOk())
    //            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
    //            .andExpect(jsonPath("$.id").value(dragQuestion.getId().intValue()))
    //            .andExpect(jsonPath("$.answerOne").value(DEFAULT_ANSWER_ONE))
    //            .andExpect(jsonPath("$.answerTwo").value(DEFAULT_ANSWER_TWO))
    //            .andExpect(jsonPath("$.answerThree").value(DEFAULT_ANSWER_THREE))
    //            .andExpect(jsonPath("$.answerFour").value(DEFAULT_ANSWER_FOUR))
    //            .andExpect(jsonPath("$.correctOrder").value(DEFAULT_CORRECT_ORDER));
    //    }
    //
    //    @Test
    //    @Transactional
    //    void getNonExistingDragQuestion() throws Exception {
    //        // Get the dragQuestion
    //        restDragQuestionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    //    }
    //
    //    @Test
    //    @Transactional
    //    void putNewDragQuestion() throws Exception {
    //        // Initialize the database
    //        dragQuestionRepository.saveAndFlush(dragQuestion);
    //
    //        int databaseSizeBeforeUpdate = dragQuestionRepository.findAll().size();
    //
    //        // Update the dragQuestion
    //        DragQuestion updatedDragQuestion = dragQuestionRepository.findById(dragQuestion.getId()).get();
    //        // Disconnect from session so that the updates on updatedDragQuestion are not directly saved in db
    //        em.detach(updatedDragQuestion);
    //        updatedDragQuestion
    //            .answerOne(UPDATED_ANSWER_ONE)
    //            .answerTwo(UPDATED_ANSWER_TWO)
    //            .answerThree(UPDATED_ANSWER_THREE)
    //            .answerFour(UPDATED_ANSWER_FOUR)
    //            .correctOrder(UPDATED_CORRECT_ORDER);
    //
    //        restDragQuestionMockMvc
    //            .perform(
    //                put(ENTITY_API_URL_ID, updatedDragQuestion.getId())
    //                    .contentType(MediaType.APPLICATION_JSON)
    //                    .content(TestUtil.convertObjectToJsonBytes(updatedDragQuestion))
    //            )
    //            .andExpect(status().isOk());
    //
    //        // Validate the DragQuestion in the database
    //        List<DragQuestion> dragQuestionList = dragQuestionRepository.findAll();
    //        assertThat(dragQuestionList).hasSize(databaseSizeBeforeUpdate);
    //        DragQuestion testDragQuestion = dragQuestionList.get(dragQuestionList.size() - 1);
    //        assertThat(testDragQuestion.getAnswerOne()).isEqualTo(UPDATED_ANSWER_ONE);
    //        assertThat(testDragQuestion.getAnswerTwo()).isEqualTo(UPDATED_ANSWER_TWO);
    //        assertThat(testDragQuestion.getAnswerThree()).isEqualTo(UPDATED_ANSWER_THREE);
    //        assertThat(testDragQuestion.getAnswerFour()).isEqualTo(UPDATED_ANSWER_FOUR);
    //        assertThat(testDragQuestion.getCorrectOrder()).isEqualTo(UPDATED_CORRECT_ORDER);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void putNonExistingDragQuestion() throws Exception {
    //        int databaseSizeBeforeUpdate = dragQuestionRepository.findAll().size();
    //        dragQuestion.setId(count.incrementAndGet());
    //
    //        // If the entity doesn't have an ID, it will throw BadRequestAlertException
    //        restDragQuestionMockMvc
    //            .perform(
    //                put(ENTITY_API_URL_ID, dragQuestion.getId())
    //                    .contentType(MediaType.APPLICATION_JSON)
    //                    .content(TestUtil.convertObjectToJsonBytes(dragQuestion))
    //            )
    //            .andExpect(status().isBadRequest());
    //
    //        // Validate the DragQuestion in the database
    //        List<DragQuestion> dragQuestionList = dragQuestionRepository.findAll();
    //        assertThat(dragQuestionList).hasSize(databaseSizeBeforeUpdate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void putWithIdMismatchDragQuestion() throws Exception {
    //        int databaseSizeBeforeUpdate = dragQuestionRepository.findAll().size();
    //        dragQuestion.setId(count.incrementAndGet());
    //
    //        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
    //        restDragQuestionMockMvc
    //            .perform(
    //                put(ENTITY_API_URL_ID, count.incrementAndGet())
    //                    .contentType(MediaType.APPLICATION_JSON)
    //                    .content(TestUtil.convertObjectToJsonBytes(dragQuestion))
    //            )
    //            .andExpect(status().isBadRequest());
    //
    //        // Validate the DragQuestion in the database
    //        List<DragQuestion> dragQuestionList = dragQuestionRepository.findAll();
    //        assertThat(dragQuestionList).hasSize(databaseSizeBeforeUpdate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void putWithMissingIdPathParamDragQuestion() throws Exception {
    //        int databaseSizeBeforeUpdate = dragQuestionRepository.findAll().size();
    //        dragQuestion.setId(count.incrementAndGet());
    //
    //        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
    //        restDragQuestionMockMvc
    //            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(dragQuestion)))
    //            .andExpect(status().isMethodNotAllowed());
    //
    //        // Validate the DragQuestion in the database
    //        List<DragQuestion> dragQuestionList = dragQuestionRepository.findAll();
    //        assertThat(dragQuestionList).hasSize(databaseSizeBeforeUpdate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void partialUpdateDragQuestionWithPatch() throws Exception {
    //        // Initialize the database
    //        dragQuestionRepository.saveAndFlush(dragQuestion);
    //
    //        int databaseSizeBeforeUpdate = dragQuestionRepository.findAll().size();
    //
    //        // Update the dragQuestion using partial update
    //        DragQuestion partialUpdatedDragQuestion = new DragQuestion();
    //        partialUpdatedDragQuestion.setId(dragQuestion.getId());
    //
    //        partialUpdatedDragQuestion.answerOne(UPDATED_ANSWER_ONE).answerTwo(UPDATED_ANSWER_TWO).correctOrder(UPDATED_CORRECT_ORDER);
    //
    //        restDragQuestionMockMvc
    //            .perform(
    //                patch(ENTITY_API_URL_ID, partialUpdatedDragQuestion.getId())
    //                    .contentType("application/merge-patch+json")
    //                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDragQuestion))
    //            )
    //            .andExpect(status().isOk());
    //
    //        // Validate the DragQuestion in the database
    //        List<DragQuestion> dragQuestionList = dragQuestionRepository.findAll();
    //        assertThat(dragQuestionList).hasSize(databaseSizeBeforeUpdate);
    //        DragQuestion testDragQuestion = dragQuestionList.get(dragQuestionList.size() - 1);
    //        assertThat(testDragQuestion.getAnswerOne()).isEqualTo(UPDATED_ANSWER_ONE);
    //        assertThat(testDragQuestion.getAnswerTwo()).isEqualTo(UPDATED_ANSWER_TWO);
    //        assertThat(testDragQuestion.getAnswerThree()).isEqualTo(DEFAULT_ANSWER_THREE);
    //        assertThat(testDragQuestion.getAnswerFour()).isEqualTo(DEFAULT_ANSWER_FOUR);
    //        assertThat(testDragQuestion.getCorrectOrder()).isEqualTo(UPDATED_CORRECT_ORDER);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void fullUpdateDragQuestionWithPatch() throws Exception {
    //        // Initialize the database
    //        dragQuestionRepository.saveAndFlush(dragQuestion);
    //
    //        int databaseSizeBeforeUpdate = dragQuestionRepository.findAll().size();
    //
    //        // Update the dragQuestion using partial update
    //        DragQuestion partialUpdatedDragQuestion = new DragQuestion();
    //        partialUpdatedDragQuestion.setId(dragQuestion.getId());
    //
    //        partialUpdatedDragQuestion
    //            .answerOne(UPDATED_ANSWER_ONE)
    //            .answerTwo(UPDATED_ANSWER_TWO)
    //            .answerThree(UPDATED_ANSWER_THREE)
    //            .answerFour(UPDATED_ANSWER_FOUR)
    //            .correctOrder(UPDATED_CORRECT_ORDER);
    //
    //        restDragQuestionMockMvc
    //            .perform(
    //                patch(ENTITY_API_URL_ID, partialUpdatedDragQuestion.getId())
    //                    .contentType("application/merge-patch+json")
    //                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedDragQuestion))
    //            )
    //            .andExpect(status().isOk());
    //
    //        // Validate the DragQuestion in the database
    //        List<DragQuestion> dragQuestionList = dragQuestionRepository.findAll();
    //        assertThat(dragQuestionList).hasSize(databaseSizeBeforeUpdate);
    //        DragQuestion testDragQuestion = dragQuestionList.get(dragQuestionList.size() - 1);
    //        assertThat(testDragQuestion.getAnswerOne()).isEqualTo(UPDATED_ANSWER_ONE);
    //        assertThat(testDragQuestion.getAnswerTwo()).isEqualTo(UPDATED_ANSWER_TWO);
    //        assertThat(testDragQuestion.getAnswerThree()).isEqualTo(UPDATED_ANSWER_THREE);
    //        assertThat(testDragQuestion.getAnswerFour()).isEqualTo(UPDATED_ANSWER_FOUR);
    //        assertThat(testDragQuestion.getCorrectOrder()).isEqualTo(UPDATED_CORRECT_ORDER);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void patchNonExistingDragQuestion() throws Exception {
    //        int databaseSizeBeforeUpdate = dragQuestionRepository.findAll().size();
    //        dragQuestion.setId(count.incrementAndGet());
    //
    //        // If the entity doesn't have an ID, it will throw BadRequestAlertException
    //        restDragQuestionMockMvc
    //            .perform(
    //                patch(ENTITY_API_URL_ID, dragQuestion.getId())
    //                    .contentType("application/merge-patch+json")
    //                    .content(TestUtil.convertObjectToJsonBytes(dragQuestion))
    //            )
    //            .andExpect(status().isBadRequest());
    //
    //        // Validate the DragQuestion in the database
    //        List<DragQuestion> dragQuestionList = dragQuestionRepository.findAll();
    //        assertThat(dragQuestionList).hasSize(databaseSizeBeforeUpdate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void patchWithIdMismatchDragQuestion() throws Exception {
    //        int databaseSizeBeforeUpdate = dragQuestionRepository.findAll().size();
    //        dragQuestion.setId(count.incrementAndGet());
    //
    //        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
    //        restDragQuestionMockMvc
    //            .perform(
    //                patch(ENTITY_API_URL_ID, count.incrementAndGet())
    //                    .contentType("application/merge-patch+json")
    //                    .content(TestUtil.convertObjectToJsonBytes(dragQuestion))
    //            )
    //            .andExpect(status().isBadRequest());
    //
    //        // Validate the DragQuestion in the database
    //        List<DragQuestion> dragQuestionList = dragQuestionRepository.findAll();
    //        assertThat(dragQuestionList).hasSize(databaseSizeBeforeUpdate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void patchWithMissingIdPathParamDragQuestion() throws Exception {
    //        int databaseSizeBeforeUpdate = dragQuestionRepository.findAll().size();
    //        dragQuestion.setId(count.incrementAndGet());
    //
    //        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
    //        restDragQuestionMockMvc
    //            .perform(
    //                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(dragQuestion))
    //            )
    //            .andExpect(status().isMethodNotAllowed());
    //
    //        // Validate the DragQuestion in the database
    //        List<DragQuestion> dragQuestionList = dragQuestionRepository.findAll();
    //        assertThat(dragQuestionList).hasSize(databaseSizeBeforeUpdate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void deleteDragQuestion() throws Exception {
    //        // Initialize the database
    //        dragQuestionRepository.saveAndFlush(dragQuestion);
    //
    //        int databaseSizeBeforeDelete = dragQuestionRepository.findAll().size();
    //
    //        // Delete the dragQuestion
    //        restDragQuestionMockMvc
    //            .perform(delete(ENTITY_API_URL_ID, dragQuestion.getId()).accept(MediaType.APPLICATION_JSON))
    //            .andExpect(status().isNoContent());
    //
    //        // Validate the database contains one less item
    //        List<DragQuestion> dragQuestionList = dragQuestionRepository.findAll();
    //        assertThat(dragQuestionList).hasSize(databaseSizeBeforeDelete - 1);
    //    }
    //
}
