package com.quizly.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.quizly.IntegrationTest;
import com.quizly.domain.OpenQuestion;
import com.quizly.repository.OpenQuestionRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link OpenQuestionResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class OpenQuestionResourceIT {
    //    private static final String DEFAULT_CORRECT = "AAAAAAAAAA";
    //    private static final String UPDATED_CORRECT = "BBBBBBBBBB";
    //
    //    private static final String ENTITY_API_URL = "/api/open-questions";
    //    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    //
    //    private static Random random = new Random();
    //    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    //
    //    @Autowired
    //    private OpenQuestionRepository openQuestionRepository;
    //
    //    @Autowired
    //    private EntityManager em;
    //
    //    @Autowired
    //    private MockMvc restOpenQuestionMockMvc;
    //
    //    private OpenQuestion openQuestion;
    //
    //    /**
    //     * Create an entity for this test.
    //     *
    //     * This is a static method, as tests for other entities might also need it,
    //     * if they test an entity which requires the current entity.
    //     */
    //    public static OpenQuestion createEntity(EntityManager em) {
    //        OpenQuestion openQuestion = new OpenQuestion().correct(DEFAULT_CORRECT);
    //        return openQuestion;
    //    }
    //
    //    /**
    //     * Create an updated entity for this test.
    //     *
    //     * This is a static method, as tests for other entities might also need it,
    //     * if they test an entity which requires the current entity.
    //     */
    //    public static OpenQuestion createUpdatedEntity(EntityManager em) {
    //        OpenQuestion openQuestion = new OpenQuestion().correct(UPDATED_CORRECT);
    //        return openQuestion;
    //    }
    //
    //    @BeforeEach
    //    public void initTest() {
    //        openQuestion = createEntity(em);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void createOpenQuestion() throws Exception {
    //        int databaseSizeBeforeCreate = openQuestionRepository.findAll().size();
    //        // Create the OpenQuestion
    //        restOpenQuestionMockMvc
    //            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(openQuestion)))
    //            .andExpect(status().isCreated());
    //
    //        // Validate the OpenQuestion in the database
    //        List<OpenQuestion> openQuestionList = openQuestionRepository.findAll();
    //        assertThat(openQuestionList).hasSize(databaseSizeBeforeCreate + 1);
    //        OpenQuestion testOpenQuestion = openQuestionList.get(openQuestionList.size() - 1);
    //        assertThat(testOpenQuestion.getCorrect()).isEqualTo(DEFAULT_CORRECT);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void createOpenQuestionWithExistingId() throws Exception {
    //        // Create the OpenQuestion with an existing ID
    //        openQuestion.setId(1L);
    //
    //        int databaseSizeBeforeCreate = openQuestionRepository.findAll().size();
    //
    //        // An entity with an existing ID cannot be created, so this API call must fail
    //        restOpenQuestionMockMvc
    //            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(openQuestion)))
    //            .andExpect(status().isBadRequest());
    //
    //        // Validate the OpenQuestion in the database
    //        List<OpenQuestion> openQuestionList = openQuestionRepository.findAll();
    //        assertThat(openQuestionList).hasSize(databaseSizeBeforeCreate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void checkCorrectIsRequired() throws Exception {
    //        int databaseSizeBeforeTest = openQuestionRepository.findAll().size();
    //        // set the field null
    //        openQuestion.setCorrect(null);
    //
    //        // Create the OpenQuestion, which fails.
    //
    //        restOpenQuestionMockMvc
    //            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(openQuestion)))
    //            .andExpect(status().isBadRequest());
    //
    //        List<OpenQuestion> openQuestionList = openQuestionRepository.findAll();
    //        assertThat(openQuestionList).hasSize(databaseSizeBeforeTest);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void getAllOpenQuestions() throws Exception {
    //        // Initialize the database
    //        openQuestionRepository.saveAndFlush(openQuestion);
    //
    //        // Get all the openQuestionList
    //        restOpenQuestionMockMvc
    //            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
    //            .andExpect(status().isOk())
    //            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
    //            .andExpect(jsonPath("$.[*].id").value(hasItem(openQuestion.getId().intValue())))
    //            .andExpect(jsonPath("$.[*].correct").value(hasItem(DEFAULT_CORRECT)));
    //    }
    //
    //    @Test
    //    @Transactional
    //    void getOpenQuestion() throws Exception {
    //        // Initialize the database
    //        openQuestionRepository.saveAndFlush(openQuestion);
    //
    //        // Get the openQuestion
    //        restOpenQuestionMockMvc
    //            .perform(get(ENTITY_API_URL_ID, openQuestion.getId()))
    //            .andExpect(status().isOk())
    //            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
    //            .andExpect(jsonPath("$.id").value(openQuestion.getId().intValue()))
    //            .andExpect(jsonPath("$.correct").value(DEFAULT_CORRECT));
    //    }
    //
    //    @Test
    //    @Transactional
    //    void getNonExistingOpenQuestion() throws Exception {
    //        // Get the openQuestion
    //        restOpenQuestionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    //    }
    //
    //    @Test
    //    @Transactional
    //    void putNewOpenQuestion() throws Exception {
    //        // Initialize the database
    //        openQuestionRepository.saveAndFlush(openQuestion);
    //
    //        int databaseSizeBeforeUpdate = openQuestionRepository.findAll().size();
    //
    //        // Update the openQuestion
    //        OpenQuestion updatedOpenQuestion = openQuestionRepository.findById(openQuestion.getId()).get();
    //        // Disconnect from session so that the updates on updatedOpenQuestion are not directly saved in db
    //        em.detach(updatedOpenQuestion);
    //        updatedOpenQuestion.correct(UPDATED_CORRECT);
    //
    //        restOpenQuestionMockMvc
    //            .perform(
    //                put(ENTITY_API_URL_ID, updatedOpenQuestion.getId())
    //                    .contentType(MediaType.APPLICATION_JSON)
    //                    .content(TestUtil.convertObjectToJsonBytes(updatedOpenQuestion))
    //            )
    //            .andExpect(status().isOk());
    //
    //        // Validate the OpenQuestion in the database
    //        List<OpenQuestion> openQuestionList = openQuestionRepository.findAll();
    //        assertThat(openQuestionList).hasSize(databaseSizeBeforeUpdate);
    //        OpenQuestion testOpenQuestion = openQuestionList.get(openQuestionList.size() - 1);
    //        assertThat(testOpenQuestion.getCorrect()).isEqualTo(UPDATED_CORRECT);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void putNonExistingOpenQuestion() throws Exception {
    //        int databaseSizeBeforeUpdate = openQuestionRepository.findAll().size();
    //        openQuestion.setId(count.incrementAndGet());
    //
    //        // If the entity doesn't have an ID, it will throw BadRequestAlertException
    //        restOpenQuestionMockMvc
    //            .perform(
    //                put(ENTITY_API_URL_ID, openQuestion.getId())
    //                    .contentType(MediaType.APPLICATION_JSON)
    //                    .content(TestUtil.convertObjectToJsonBytes(openQuestion))
    //            )
    //            .andExpect(status().isBadRequest());
    //
    //        // Validate the OpenQuestion in the database
    //        List<OpenQuestion> openQuestionList = openQuestionRepository.findAll();
    //        assertThat(openQuestionList).hasSize(databaseSizeBeforeUpdate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void putWithIdMismatchOpenQuestion() throws Exception {
    //        int databaseSizeBeforeUpdate = openQuestionRepository.findAll().size();
    //        openQuestion.setId(count.incrementAndGet());
    //
    //        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
    //        restOpenQuestionMockMvc
    //            .perform(
    //                put(ENTITY_API_URL_ID, count.incrementAndGet())
    //                    .contentType(MediaType.APPLICATION_JSON)
    //                    .content(TestUtil.convertObjectToJsonBytes(openQuestion))
    //            )
    //            .andExpect(status().isBadRequest());
    //
    //        // Validate the OpenQuestion in the database
    //        List<OpenQuestion> openQuestionList = openQuestionRepository.findAll();
    //        assertThat(openQuestionList).hasSize(databaseSizeBeforeUpdate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void putWithMissingIdPathParamOpenQuestion() throws Exception {
    //        int databaseSizeBeforeUpdate = openQuestionRepository.findAll().size();
    //        openQuestion.setId(count.incrementAndGet());
    //
    //        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
    //        restOpenQuestionMockMvc
    //            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(openQuestion)))
    //            .andExpect(status().isMethodNotAllowed());
    //
    //        // Validate the OpenQuestion in the database
    //        List<OpenQuestion> openQuestionList = openQuestionRepository.findAll();
    //        assertThat(openQuestionList).hasSize(databaseSizeBeforeUpdate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void partialUpdateOpenQuestionWithPatch() throws Exception {
    //        // Initialize the database
    //        openQuestionRepository.saveAndFlush(openQuestion);
    //
    //        int databaseSizeBeforeUpdate = openQuestionRepository.findAll().size();
    //
    //        // Update the openQuestion using partial update
    //        OpenQuestion partialUpdatedOpenQuestion = new OpenQuestion();
    //        partialUpdatedOpenQuestion.setId(openQuestion.getId());
    //
    //        partialUpdatedOpenQuestion.correct(UPDATED_CORRECT);
    //
    //        restOpenQuestionMockMvc
    //            .perform(
    //                patch(ENTITY_API_URL_ID, partialUpdatedOpenQuestion.getId())
    //                    .contentType("application/merge-patch+json")
    //                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOpenQuestion))
    //            )
    //            .andExpect(status().isOk());
    //
    //        // Validate the OpenQuestion in the database
    //        List<OpenQuestion> openQuestionList = openQuestionRepository.findAll();
    //        assertThat(openQuestionList).hasSize(databaseSizeBeforeUpdate);
    //        OpenQuestion testOpenQuestion = openQuestionList.get(openQuestionList.size() - 1);
    //        assertThat(testOpenQuestion.getCorrect()).isEqualTo(UPDATED_CORRECT);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void fullUpdateOpenQuestionWithPatch() throws Exception {
    //        // Initialize the database
    //        openQuestionRepository.saveAndFlush(openQuestion);
    //
    //        int databaseSizeBeforeUpdate = openQuestionRepository.findAll().size();
    //
    //        // Update the openQuestion using partial update
    //        OpenQuestion partialUpdatedOpenQuestion = new OpenQuestion();
    //        partialUpdatedOpenQuestion.setId(openQuestion.getId());
    //
    //        partialUpdatedOpenQuestion.correct(UPDATED_CORRECT);
    //
    //        restOpenQuestionMockMvc
    //            .perform(
    //                patch(ENTITY_API_URL_ID, partialUpdatedOpenQuestion.getId())
    //                    .contentType("application/merge-patch+json")
    //                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedOpenQuestion))
    //            )
    //            .andExpect(status().isOk());
    //
    //        // Validate the OpenQuestion in the database
    //        List<OpenQuestion> openQuestionList = openQuestionRepository.findAll();
    //        assertThat(openQuestionList).hasSize(databaseSizeBeforeUpdate);
    //        OpenQuestion testOpenQuestion = openQuestionList.get(openQuestionList.size() - 1);
    //        assertThat(testOpenQuestion.getCorrect()).isEqualTo(UPDATED_CORRECT);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void patchNonExistingOpenQuestion() throws Exception {
    //        int databaseSizeBeforeUpdate = openQuestionRepository.findAll().size();
    //        openQuestion.setId(count.incrementAndGet());
    //
    //        // If the entity doesn't have an ID, it will throw BadRequestAlertException
    //        restOpenQuestionMockMvc
    //            .perform(
    //                patch(ENTITY_API_URL_ID, openQuestion.getId())
    //                    .contentType("application/merge-patch+json")
    //                    .content(TestUtil.convertObjectToJsonBytes(openQuestion))
    //            )
    //            .andExpect(status().isBadRequest());
    //
    //        // Validate the OpenQuestion in the database
    //        List<OpenQuestion> openQuestionList = openQuestionRepository.findAll();
    //        assertThat(openQuestionList).hasSize(databaseSizeBeforeUpdate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void patchWithIdMismatchOpenQuestion() throws Exception {
    //        int databaseSizeBeforeUpdate = openQuestionRepository.findAll().size();
    //        openQuestion.setId(count.incrementAndGet());
    //
    //        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
    //        restOpenQuestionMockMvc
    //            .perform(
    //                patch(ENTITY_API_URL_ID, count.incrementAndGet())
    //                    .contentType("application/merge-patch+json")
    //                    .content(TestUtil.convertObjectToJsonBytes(openQuestion))
    //            )
    //            .andExpect(status().isBadRequest());
    //
    //        // Validate the OpenQuestion in the database
    //        List<OpenQuestion> openQuestionList = openQuestionRepository.findAll();
    //        assertThat(openQuestionList).hasSize(databaseSizeBeforeUpdate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void patchWithMissingIdPathParamOpenQuestion() throws Exception {
    //        int databaseSizeBeforeUpdate = openQuestionRepository.findAll().size();
    //        openQuestion.setId(count.incrementAndGet());
    //
    //        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
    //        restOpenQuestionMockMvc
    //            .perform(
    //                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(openQuestion))
    //            )
    //            .andExpect(status().isMethodNotAllowed());
    //
    //        // Validate the OpenQuestion in the database
    //        List<OpenQuestion> openQuestionList = openQuestionRepository.findAll();
    //        assertThat(openQuestionList).hasSize(databaseSizeBeforeUpdate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void deleteOpenQuestion() throws Exception {
    //        // Initialize the database
    //        openQuestionRepository.saveAndFlush(openQuestion);
    //
    //        int databaseSizeBeforeDelete = openQuestionRepository.findAll().size();
    //
    //        // Delete the openQuestion
    //        restOpenQuestionMockMvc
    //            .perform(delete(ENTITY_API_URL_ID, openQuestion.getId()).accept(MediaType.APPLICATION_JSON))
    //            .andExpect(status().isNoContent());
    //
    //        // Validate the database contains one less item
    //        List<OpenQuestion> openQuestionList = openQuestionRepository.findAll();
    //        assertThat(openQuestionList).hasSize(databaseSizeBeforeDelete - 1);
    //    }
}
