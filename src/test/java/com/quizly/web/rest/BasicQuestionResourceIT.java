package com.quizly.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.quizly.IntegrationTest;
import com.quizly.domain.BasicQuestion;
import com.quizly.repository.BasicQuestionRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link BasicQuestionResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class BasicQuestionResourceIT {
    //    private static final String DEFAULT_ANSWER_ONE = "AAAAAAAAAA";
    //    private static final String UPDATED_ANSWER_ONE = "BBBBBBBBBB";
    //
    //    private static final String DEFAULT_ANSWER_TWO = "AAAAAAAAAA";
    //    private static final String UPDATED_ANSWER_TWO = "BBBBBBBBBB";
    //
    //    private static final String DEFAULT_ANSWER_THREE = "AAAAAAAAAA";
    //    private static final String UPDATED_ANSWER_THREE = "BBBBBBBBBB";
    //
    //    private static final String DEFAULT_ANSWER_FOUR = "AAAAAAAAAA";
    //    private static final String UPDATED_ANSWER_FOUR = "BBBBBBBBBB";
    //
    //    private static final Integer DEFAULT_CORRECT = 1;
    //    private static final Integer UPDATED_CORRECT = 2;
    //
    //    private static final String ENTITY_API_URL = "/api/basic-questions";
    //    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";
    //
    //    private static Random random = new Random();
    //    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    //
    //    @Autowired
    //    private BasicQuestionRepository basicQuestionRepository;
    //
    //    @Autowired
    //    private EntityManager em;
    //
    //    @Autowired
    //    private MockMvc restBasicQuestionMockMvc;
    //
    //    private BasicQuestion basicQuestion;
    //
    //    /**
    //     * Create an entity for this test.
    //     *
    //     * This is a static method, as tests for other entities might also need it,
    //     * if they test an entity which requires the current entity.
    //     */
    //    public static BasicQuestion createEntity(EntityManager em) {
    //        BasicQuestion basicQuestion = new BasicQuestion()
    //            .answerOne(DEFAULT_ANSWER_ONE)
    //            .answerTwo(DEFAULT_ANSWER_TWO)
    //            .answerThree(DEFAULT_ANSWER_THREE)
    //            .answerFour(DEFAULT_ANSWER_FOUR)
    //            .correct(DEFAULT_CORRECT);
    //        return basicQuestion;
    //    }
    //
    //    /**
    //     * Create an updated entity for this test.
    //     *
    //     * This is a static method, as tests for other entities might also need it,
    //     * if they test an entity which requires the current entity.
    //     */
    //    public static BasicQuestion createUpdatedEntity(EntityManager em) {
    //        BasicQuestion basicQuestion = new BasicQuestion()
    //            .answerOne(UPDATED_ANSWER_ONE)
    //            .answerTwo(UPDATED_ANSWER_TWO)
    //            .answerThree(UPDATED_ANSWER_THREE)
    //            .answerFour(UPDATED_ANSWER_FOUR)
    //            .correct(UPDATED_CORRECT);
    //        return basicQuestion;
    //    }
    //
    //    @BeforeEach
    //    public void initTest() {
    //        basicQuestion = createEntity(em);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void createBasicQuestion() throws Exception {
    //        int databaseSizeBeforeCreate = basicQuestionRepository.findAll().size();
    //        // Create the BasicQuestion
    //        restBasicQuestionMockMvc
    //            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(basicQuestion)))
    //            .andExpect(status().isCreated());
    //
    //        // Validate the BasicQuestion in the database
    //        List<BasicQuestion> basicQuestionList = basicQuestionRepository.findAll();
    //        assertThat(basicQuestionList).hasSize(databaseSizeBeforeCreate + 1);
    //        BasicQuestion testBasicQuestion = basicQuestionList.get(basicQuestionList.size() - 1);
    //        assertThat(testBasicQuestion.getAnswerOne()).isEqualTo(DEFAULT_ANSWER_ONE);
    //        assertThat(testBasicQuestion.getAnswerTwo()).isEqualTo(DEFAULT_ANSWER_TWO);
    //        assertThat(testBasicQuestion.getAnswerThree()).isEqualTo(DEFAULT_ANSWER_THREE);
    //        assertThat(testBasicQuestion.getAnswerFour()).isEqualTo(DEFAULT_ANSWER_FOUR);
    //        assertThat(testBasicQuestion.getCorrect()).isEqualTo(DEFAULT_CORRECT);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void createBasicQuestionWithExistingId() throws Exception {
    //        // Create the BasicQuestion with an existing ID
    //        basicQuestion.setId(1L);
    //
    //        int databaseSizeBeforeCreate = basicQuestionRepository.findAll().size();
    //
    //        // An entity with an existing ID cannot be created, so this API call must fail
    //        restBasicQuestionMockMvc
    //            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(basicQuestion)))
    //            .andExpect(status().isBadRequest());
    //
    //        // Validate the BasicQuestion in the database
    //        List<BasicQuestion> basicQuestionList = basicQuestionRepository.findAll();
    //        assertThat(basicQuestionList).hasSize(databaseSizeBeforeCreate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void checkAnswerOneIsRequired() throws Exception {
    //        int databaseSizeBeforeTest = basicQuestionRepository.findAll().size();
    //        // set the field null
    //        basicQuestion.setAnswerOne(null);
    //
    //        // Create the BasicQuestion, which fails.
    //
    //        restBasicQuestionMockMvc
    //            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(basicQuestion)))
    //            .andExpect(status().isBadRequest());
    //
    //        List<BasicQuestion> basicQuestionList = basicQuestionRepository.findAll();
    //        assertThat(basicQuestionList).hasSize(databaseSizeBeforeTest);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void checkAnswerTwoIsRequired() throws Exception {
    //        int databaseSizeBeforeTest = basicQuestionRepository.findAll().size();
    //        // set the field null
    //        basicQuestion.setAnswerTwo(null);
    //
    //        // Create the BasicQuestion, which fails.
    //
    //        restBasicQuestionMockMvc
    //            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(basicQuestion)))
    //            .andExpect(status().isBadRequest());
    //
    //        List<BasicQuestion> basicQuestionList = basicQuestionRepository.findAll();
    //        assertThat(basicQuestionList).hasSize(databaseSizeBeforeTest);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void checkCorrectIsRequired() throws Exception {
    //        int databaseSizeBeforeTest = basicQuestionRepository.findAll().size();
    //        // set the field null
    //        basicQuestion.setCorrect(null);
    //
    //        // Create the BasicQuestion, which fails.
    //
    //        restBasicQuestionMockMvc
    //            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(basicQuestion)))
    //            .andExpect(status().isBadRequest());
    //
    //        List<BasicQuestion> basicQuestionList = basicQuestionRepository.findAll();
    //        assertThat(basicQuestionList).hasSize(databaseSizeBeforeTest);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void getAllBasicQuestions() throws Exception {
    //        // Initialize the database
    //        basicQuestionRepository.saveAndFlush(basicQuestion);
    //
    //        // Get all the basicQuestionList
    //        restBasicQuestionMockMvc
    //            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
    //            .andExpect(status().isOk())
    //            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
    //            .andExpect(jsonPath("$.[*].id").value(hasItem(basicQuestion.getId().intValue())))
    //            .andExpect(jsonPath("$.[*].answerOne").value(hasItem(DEFAULT_ANSWER_ONE)))
    //            .andExpect(jsonPath("$.[*].answerTwo").value(hasItem(DEFAULT_ANSWER_TWO)))
    //            .andExpect(jsonPath("$.[*].answerThree").value(hasItem(DEFAULT_ANSWER_THREE)))
    //            .andExpect(jsonPath("$.[*].answerFour").value(hasItem(DEFAULT_ANSWER_FOUR)))
    //            .andExpect(jsonPath("$.[*].correct").value(hasItem(DEFAULT_CORRECT)));
    //    }
    //
    //    @Test
    //    @Transactional
    //    void getBasicQuestion() throws Exception {
    //        // Initialize the database
    //        basicQuestionRepository.saveAndFlush(basicQuestion);
    //
    //        // Get the basicQuestion
    //        restBasicQuestionMockMvc
    //            .perform(get(ENTITY_API_URL_ID, basicQuestion.getId()))
    //            .andExpect(status().isOk())
    //            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
    //            .andExpect(jsonPath("$.id").value(basicQuestion.getId().intValue()))
    //            .andExpect(jsonPath("$.answerOne").value(DEFAULT_ANSWER_ONE))
    //            .andExpect(jsonPath("$.answerTwo").value(DEFAULT_ANSWER_TWO))
    //            .andExpect(jsonPath("$.answerThree").value(DEFAULT_ANSWER_THREE))
    //            .andExpect(jsonPath("$.answerFour").value(DEFAULT_ANSWER_FOUR))
    //            .andExpect(jsonPath("$.correct").value(DEFAULT_CORRECT));
    //    }
    //
    //    @Test
    //    @Transactional
    //    void getNonExistingBasicQuestion() throws Exception {
    //        // Get the basicQuestion
    //        restBasicQuestionMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    //    }
    //
    //    @Test
    //    @Transactional
    //    void putNewBasicQuestion() throws Exception {
    //        // Initialize the database
    //        basicQuestionRepository.saveAndFlush(basicQuestion);
    //
    //        int databaseSizeBeforeUpdate = basicQuestionRepository.findAll().size();
    //
    //        // Update the basicQuestion
    //        BasicQuestion updatedBasicQuestion = basicQuestionRepository.findById(basicQuestion.getId()).get();
    //        // Disconnect from session so that the updates on updatedBasicQuestion are not directly saved in db
    //        em.detach(updatedBasicQuestion);
    //        updatedBasicQuestion
    //            .answerOne(UPDATED_ANSWER_ONE)
    //            .answerTwo(UPDATED_ANSWER_TWO)
    //            .answerThree(UPDATED_ANSWER_THREE)
    //            .answerFour(UPDATED_ANSWER_FOUR)
    //            .correct(UPDATED_CORRECT);
    //
    //        restBasicQuestionMockMvc
    //            .perform(
    //                put(ENTITY_API_URL_ID, updatedBasicQuestion.getId())
    //                    .contentType(MediaType.APPLICATION_JSON)
    //                    .content(TestUtil.convertObjectToJsonBytes(updatedBasicQuestion))
    //            )
    //            .andExpect(status().isOk());
    //
    //        // Validate the BasicQuestion in the database
    //        List<BasicQuestion> basicQuestionList = basicQuestionRepository.findAll();
    //        assertThat(basicQuestionList).hasSize(databaseSizeBeforeUpdate);
    //        BasicQuestion testBasicQuestion = basicQuestionList.get(basicQuestionList.size() - 1);
    //        assertThat(testBasicQuestion.getAnswerOne()).isEqualTo(UPDATED_ANSWER_ONE);
    //        assertThat(testBasicQuestion.getAnswerTwo()).isEqualTo(UPDATED_ANSWER_TWO);
    //        assertThat(testBasicQuestion.getAnswerThree()).isEqualTo(UPDATED_ANSWER_THREE);
    //        assertThat(testBasicQuestion.getAnswerFour()).isEqualTo(UPDATED_ANSWER_FOUR);
    //        assertThat(testBasicQuestion.getCorrect()).isEqualTo(UPDATED_CORRECT);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void putNonExistingBasicQuestion() throws Exception {
    //        int databaseSizeBeforeUpdate = basicQuestionRepository.findAll().size();
    //        basicQuestion.setId(count.incrementAndGet());
    //
    //        // If the entity doesn't have an ID, it will throw BadRequestAlertException
    //        restBasicQuestionMockMvc
    //            .perform(
    //                put(ENTITY_API_URL_ID, basicQuestion.getId())
    //                    .contentType(MediaType.APPLICATION_JSON)
    //                    .content(TestUtil.convertObjectToJsonBytes(basicQuestion))
    //            )
    //            .andExpect(status().isBadRequest());
    //
    //        // Validate the BasicQuestion in the database
    //        List<BasicQuestion> basicQuestionList = basicQuestionRepository.findAll();
    //        assertThat(basicQuestionList).hasSize(databaseSizeBeforeUpdate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void putWithIdMismatchBasicQuestion() throws Exception {
    //        int databaseSizeBeforeUpdate = basicQuestionRepository.findAll().size();
    //        basicQuestion.setId(count.incrementAndGet());
    //
    //        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
    //        restBasicQuestionMockMvc
    //            .perform(
    //                put(ENTITY_API_URL_ID, count.incrementAndGet())
    //                    .contentType(MediaType.APPLICATION_JSON)
    //                    .content(TestUtil.convertObjectToJsonBytes(basicQuestion))
    //            )
    //            .andExpect(status().isBadRequest());
    //
    //        // Validate the BasicQuestion in the database
    //        List<BasicQuestion> basicQuestionList = basicQuestionRepository.findAll();
    //        assertThat(basicQuestionList).hasSize(databaseSizeBeforeUpdate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void putWithMissingIdPathParamBasicQuestion() throws Exception {
    //        int databaseSizeBeforeUpdate = basicQuestionRepository.findAll().size();
    //        basicQuestion.setId(count.incrementAndGet());
    //
    //        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
    //        restBasicQuestionMockMvc
    //            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(basicQuestion)))
    //            .andExpect(status().isMethodNotAllowed());
    //
    //        // Validate the BasicQuestion in the database
    //        List<BasicQuestion> basicQuestionList = basicQuestionRepository.findAll();
    //        assertThat(basicQuestionList).hasSize(databaseSizeBeforeUpdate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void partialUpdateBasicQuestionWithPatch() throws Exception {
    //        // Initialize the database
    //        basicQuestionRepository.saveAndFlush(basicQuestion);
    //
    //        int databaseSizeBeforeUpdate = basicQuestionRepository.findAll().size();
    //
    //        // Update the basicQuestion using partial update
    //        BasicQuestion partialUpdatedBasicQuestion = new BasicQuestion();
    //        partialUpdatedBasicQuestion.setId(basicQuestion.getId());
    //
    //        partialUpdatedBasicQuestion
    //            .answerOne(UPDATED_ANSWER_ONE)
    //            .answerThree(UPDATED_ANSWER_THREE)
    //            .answerFour(UPDATED_ANSWER_FOUR)
    //            .correct(UPDATED_CORRECT);
    //
    //        restBasicQuestionMockMvc
    //            .perform(
    //                patch(ENTITY_API_URL_ID, partialUpdatedBasicQuestion.getId())
    //                    .contentType("application/merge-patch+json")
    //                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBasicQuestion))
    //            )
    //            .andExpect(status().isOk());
    //
    //        // Validate the BasicQuestion in the database
    //        List<BasicQuestion> basicQuestionList = basicQuestionRepository.findAll();
    //        assertThat(basicQuestionList).hasSize(databaseSizeBeforeUpdate);
    //        BasicQuestion testBasicQuestion = basicQuestionList.get(basicQuestionList.size() - 1);
    //        assertThat(testBasicQuestion.getAnswerOne()).isEqualTo(UPDATED_ANSWER_ONE);
    //        assertThat(testBasicQuestion.getAnswerTwo()).isEqualTo(DEFAULT_ANSWER_TWO);
    //        assertThat(testBasicQuestion.getAnswerThree()).isEqualTo(UPDATED_ANSWER_THREE);
    //        assertThat(testBasicQuestion.getAnswerFour()).isEqualTo(UPDATED_ANSWER_FOUR);
    //        assertThat(testBasicQuestion.getCorrect()).isEqualTo(UPDATED_CORRECT);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void fullUpdateBasicQuestionWithPatch() throws Exception {
    //        // Initialize the database
    //        basicQuestionRepository.saveAndFlush(basicQuestion);
    //
    //        int databaseSizeBeforeUpdate = basicQuestionRepository.findAll().size();
    //
    //        // Update the basicQuestion using partial update
    //        BasicQuestion partialUpdatedBasicQuestion = new BasicQuestion();
    //        partialUpdatedBasicQuestion.setId(basicQuestion.getId());
    //
    //        partialUpdatedBasicQuestion
    //            .answerOne(UPDATED_ANSWER_ONE)
    //            .answerTwo(UPDATED_ANSWER_TWO)
    //            .answerThree(UPDATED_ANSWER_THREE)
    //            .answerFour(UPDATED_ANSWER_FOUR)
    //            .correct(UPDATED_CORRECT);
    //
    //        restBasicQuestionMockMvc
    //            .perform(
    //                patch(ENTITY_API_URL_ID, partialUpdatedBasicQuestion.getId())
    //                    .contentType("application/merge-patch+json")
    //                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedBasicQuestion))
    //            )
    //            .andExpect(status().isOk());
    //
    //        // Validate the BasicQuestion in the database
    //        List<BasicQuestion> basicQuestionList = basicQuestionRepository.findAll();
    //        assertThat(basicQuestionList).hasSize(databaseSizeBeforeUpdate);
    //        BasicQuestion testBasicQuestion = basicQuestionList.get(basicQuestionList.size() - 1);
    //        assertThat(testBasicQuestion.getAnswerOne()).isEqualTo(UPDATED_ANSWER_ONE);
    //        assertThat(testBasicQuestion.getAnswerTwo()).isEqualTo(UPDATED_ANSWER_TWO);
    //        assertThat(testBasicQuestion.getAnswerThree()).isEqualTo(UPDATED_ANSWER_THREE);
    //        assertThat(testBasicQuestion.getAnswerFour()).isEqualTo(UPDATED_ANSWER_FOUR);
    //        assertThat(testBasicQuestion.getCorrect()).isEqualTo(UPDATED_CORRECT);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void patchNonExistingBasicQuestion() throws Exception {
    //        int databaseSizeBeforeUpdate = basicQuestionRepository.findAll().size();
    //        basicQuestion.setId(count.incrementAndGet());
    //
    //        // If the entity doesn't have an ID, it will throw BadRequestAlertException
    //        restBasicQuestionMockMvc
    //            .perform(
    //                patch(ENTITY_API_URL_ID, basicQuestion.getId())
    //                    .contentType("application/merge-patch+json")
    //                    .content(TestUtil.convertObjectToJsonBytes(basicQuestion))
    //            )
    //            .andExpect(status().isBadRequest());
    //
    //        // Validate the BasicQuestion in the database
    //        List<BasicQuestion> basicQuestionList = basicQuestionRepository.findAll();
    //        assertThat(basicQuestionList).hasSize(databaseSizeBeforeUpdate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void patchWithIdMismatchBasicQuestion() throws Exception {
    //        int databaseSizeBeforeUpdate = basicQuestionRepository.findAll().size();
    //        basicQuestion.setId(count.incrementAndGet());
    //
    //        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
    //        restBasicQuestionMockMvc
    //            .perform(
    //                patch(ENTITY_API_URL_ID, count.incrementAndGet())
    //                    .contentType("application/merge-patch+json")
    //                    .content(TestUtil.convertObjectToJsonBytes(basicQuestion))
    //            )
    //            .andExpect(status().isBadRequest());
    //
    //        // Validate the BasicQuestion in the database
    //        List<BasicQuestion> basicQuestionList = basicQuestionRepository.findAll();
    //        assertThat(basicQuestionList).hasSize(databaseSizeBeforeUpdate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void patchWithMissingIdPathParamBasicQuestion() throws Exception {
    //        int databaseSizeBeforeUpdate = basicQuestionRepository.findAll().size();
    //        basicQuestion.setId(count.incrementAndGet());
    //
    //        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
    //        restBasicQuestionMockMvc
    //            .perform(
    //                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(basicQuestion))
    //            )
    //            .andExpect(status().isMethodNotAllowed());
    //
    //        // Validate the BasicQuestion in the database
    //        List<BasicQuestion> basicQuestionList = basicQuestionRepository.findAll();
    //        assertThat(basicQuestionList).hasSize(databaseSizeBeforeUpdate);
    //    }
    //
    //    @Test
    //    @Transactional
    //    void deleteBasicQuestion() throws Exception {
    //        // Initialize the database
    //        basicQuestionRepository.saveAndFlush(basicQuestion);
    //
    //        int databaseSizeBeforeDelete = basicQuestionRepository.findAll().size();
    //
    //        // Delete the basicQuestion
    //        restBasicQuestionMockMvc
    //            .perform(delete(ENTITY_API_URL_ID, basicQuestion.getId()).accept(MediaType.APPLICATION_JSON))
    //            .andExpect(status().isNoContent());
    //
    //        // Validate the database contains one less item
    //        List<BasicQuestion> basicQuestionList = basicQuestionRepository.findAll();
    //        assertThat(basicQuestionList).hasSize(databaseSizeBeforeDelete - 1);
    //    }
}
