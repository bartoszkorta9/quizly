package com.quizly.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.quizly.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class DragQuestionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DragQuestion.class);
        DragQuestion dragQuestion1 = new DragQuestion();
        dragQuestion1.setId(1L);
        DragQuestion dragQuestion2 = new DragQuestion();
        dragQuestion2.setId(dragQuestion1.getId());
        assertThat(dragQuestion1).isEqualTo(dragQuestion2);
        dragQuestion2.setId(2L);
        assertThat(dragQuestion1).isNotEqualTo(dragQuestion2);
        dragQuestion1.setId(null);
        assertThat(dragQuestion1).isNotEqualTo(dragQuestion2);
    }
}
