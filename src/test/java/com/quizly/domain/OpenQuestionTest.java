package com.quizly.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.quizly.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class OpenQuestionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OpenQuestion.class);
        OpenQuestion openQuestion1 = new OpenQuestion();
        openQuestion1.setId(1L);
        OpenQuestion openQuestion2 = new OpenQuestion();
        openQuestion2.setId(openQuestion1.getId());
        assertThat(openQuestion1).isEqualTo(openQuestion2);
        openQuestion2.setId(2L);
        assertThat(openQuestion1).isNotEqualTo(openQuestion2);
        openQuestion1.setId(null);
        assertThat(openQuestion1).isNotEqualTo(openQuestion2);
    }
}
