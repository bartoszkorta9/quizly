package com.quizly.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.quizly.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class BasicQuestionTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BasicQuestion.class);
        BasicQuestion basicQuestion1 = new BasicQuestion();
        basicQuestion1.setId(1L);
        BasicQuestion basicQuestion2 = new BasicQuestion();
        basicQuestion2.setId(basicQuestion1.getId());
        assertThat(basicQuestion1).isEqualTo(basicQuestion2);
        basicQuestion2.setId(2L);
        assertThat(basicQuestion1).isNotEqualTo(basicQuestion2);
        basicQuestion1.setId(null);
        assertThat(basicQuestion1).isNotEqualTo(basicQuestion2);
    }
}
