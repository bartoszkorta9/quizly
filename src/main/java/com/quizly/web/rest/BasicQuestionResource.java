package com.quizly.web.rest;

import com.quizly.domain.BasicQuestion;
import com.quizly.repository.BasicQuestionRepository;
import com.quizly.service.BasicQuestionService;
import com.quizly.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.quizly.domain.BasicQuestion}.
 */
@RestController
@RequestMapping("/api")
public class BasicQuestionResource {

    private final Logger log = LoggerFactory.getLogger(BasicQuestionResource.class);

    private static final String ENTITY_NAME = "basicQuestion";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BasicQuestionService basicQuestionService;

    private final BasicQuestionRepository basicQuestionRepository;

    public BasicQuestionResource(BasicQuestionService basicQuestionService, BasicQuestionRepository basicQuestionRepository) {
        this.basicQuestionService = basicQuestionService;
        this.basicQuestionRepository = basicQuestionRepository;
    }

    /**
     * {@code POST  /basic-questions} : Create a new basicQuestion.
     *
     * @param basicQuestion the basicQuestion to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new basicQuestion, or with status {@code 400 (Bad Request)} if the basicQuestion has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/basic-questions")
    public ResponseEntity<BasicQuestion> createBasicQuestion(@Valid @RequestBody BasicQuestion basicQuestion) throws URISyntaxException {
        log.debug("REST request to save BasicQuestion : {}", basicQuestion);
        if (basicQuestion.getId() != null) {
            throw new BadRequestAlertException("A new basicQuestion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BasicQuestion result = basicQuestionService.save(basicQuestion);
        return ResponseEntity
            .created(new URI("/api/basic-questions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /basic-questions/:id} : Updates an existing basicQuestion.
     *
     * @param id the id of the basicQuestion to save.
     * @param basicQuestion the basicQuestion to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated basicQuestion,
     * or with status {@code 400 (Bad Request)} if the basicQuestion is not valid,
     * or with status {@code 500 (Internal Server Error)} if the basicQuestion couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/basic-questions/{id}")
    public ResponseEntity<BasicQuestion> updateBasicQuestion(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody BasicQuestion basicQuestion
    ) throws URISyntaxException {
        log.debug("REST request to update BasicQuestion : {}, {}", id, basicQuestion);
        if (basicQuestion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, basicQuestion.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!basicQuestionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        BasicQuestion result = basicQuestionService.save(basicQuestion);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, basicQuestion.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /basic-questions/:id} : Partial updates given fields of an existing basicQuestion, field will ignore if it is null
     *
     * @param id the id of the basicQuestion to save.
     * @param basicQuestion the basicQuestion to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated basicQuestion,
     * or with status {@code 400 (Bad Request)} if the basicQuestion is not valid,
     * or with status {@code 404 (Not Found)} if the basicQuestion is not found,
     * or with status {@code 500 (Internal Server Error)} if the basicQuestion couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/basic-questions/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<BasicQuestion> partialUpdateBasicQuestion(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody BasicQuestion basicQuestion
    ) throws URISyntaxException {
        log.debug("REST request to partial update BasicQuestion partially : {}, {}", id, basicQuestion);
        if (basicQuestion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, basicQuestion.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!basicQuestionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<BasicQuestion> result = basicQuestionService.partialUpdate(basicQuestion);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, basicQuestion.getId().toString())
        );
    }

    /**
     * {@code GET  /basic-questions} : get all the basicQuestions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of basicQuestions in body.
     */
    @GetMapping("/basic-questions")
    public List<BasicQuestion> getAllBasicQuestions() {
        log.debug("REST request to get all BasicQuestions");
        return basicQuestionService.findAll();
    }

    /**
     * {@code GET  /basic-questions/:id} : get the "id" basicQuestion.
     *
     * @param id the id of the basicQuestion to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the basicQuestion, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/basic-questions/{id}")
    public ResponseEntity<BasicQuestion> getBasicQuestion(@PathVariable Long id) {
        log.debug("REST request to get BasicQuestion : {}", id);
        Optional<BasicQuestion> basicQuestion = basicQuestionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(basicQuestion);
    }

    /**
     * {@code DELETE  /basic-questions/:id} : delete the "id" basicQuestion.
     *
     * @param id the id of the basicQuestion to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/basic-questions/{id}")
    public ResponseEntity<Void> deleteBasicQuestion(@PathVariable Long id) {
        log.debug("REST request to delete BasicQuestion : {}", id);
        basicQuestionService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
