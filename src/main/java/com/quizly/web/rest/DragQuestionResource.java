package com.quizly.web.rest;

import com.quizly.domain.DragQuestion;
import com.quizly.repository.DragQuestionRepository;
import com.quizly.service.DragQuestionService;
import com.quizly.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.quizly.domain.DragQuestion}.
 */
@RestController
@RequestMapping("/api")
public class DragQuestionResource {

    private final Logger log = LoggerFactory.getLogger(DragQuestionResource.class);

    private static final String ENTITY_NAME = "dragQuestion";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DragQuestionService dragQuestionService;

    private final DragQuestionRepository dragQuestionRepository;

    public DragQuestionResource(DragQuestionService dragQuestionService, DragQuestionRepository dragQuestionRepository) {
        this.dragQuestionService = dragQuestionService;
        this.dragQuestionRepository = dragQuestionRepository;
    }

    /**
     * {@code POST  /drag-questions} : Create a new dragQuestion.
     *
     * @param dragQuestion the dragQuestion to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new dragQuestion, or with status {@code 400 (Bad Request)} if the dragQuestion has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/drag-questions")
    public ResponseEntity<DragQuestion> createDragQuestion(@Valid @RequestBody DragQuestion dragQuestion) throws URISyntaxException {
        log.debug("REST request to save DragQuestion : {}", dragQuestion);
        if (dragQuestion.getId() != null) {
            throw new BadRequestAlertException("A new dragQuestion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DragQuestion result = dragQuestionService.save(dragQuestion);
        return ResponseEntity
            .created(new URI("/api/drag-questions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /drag-questions/:id} : Updates an existing dragQuestion.
     *
     * @param id the id of the dragQuestion to save.
     * @param dragQuestion the dragQuestion to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dragQuestion,
     * or with status {@code 400 (Bad Request)} if the dragQuestion is not valid,
     * or with status {@code 500 (Internal Server Error)} if the dragQuestion couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/drag-questions/{id}")
    public ResponseEntity<DragQuestion> updateDragQuestion(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody DragQuestion dragQuestion
    ) throws URISyntaxException {
        log.debug("REST request to update DragQuestion : {}, {}", id, dragQuestion);
        if (dragQuestion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dragQuestion.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dragQuestionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        DragQuestion result = dragQuestionService.save(dragQuestion);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dragQuestion.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /drag-questions/:id} : Partial updates given fields of an existing dragQuestion, field will ignore if it is null
     *
     * @param id the id of the dragQuestion to save.
     * @param dragQuestion the dragQuestion to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated dragQuestion,
     * or with status {@code 400 (Bad Request)} if the dragQuestion is not valid,
     * or with status {@code 404 (Not Found)} if the dragQuestion is not found,
     * or with status {@code 500 (Internal Server Error)} if the dragQuestion couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/drag-questions/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<DragQuestion> partialUpdateDragQuestion(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody DragQuestion dragQuestion
    ) throws URISyntaxException {
        log.debug("REST request to partial update DragQuestion partially : {}, {}", id, dragQuestion);
        if (dragQuestion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, dragQuestion.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!dragQuestionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<DragQuestion> result = dragQuestionService.partialUpdate(dragQuestion);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, dragQuestion.getId().toString())
        );
    }

    /**
     * {@code GET  /drag-questions} : get all the dragQuestions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of dragQuestions in body.
     */
    @GetMapping("/drag-questions")
    public List<DragQuestion> getAllDragQuestions() {
        log.debug("REST request to get all DragQuestions");
        return dragQuestionService.findAll();
    }

    /**
     * {@code GET  /drag-questions/:id} : get the "id" dragQuestion.
     *
     * @param id the id of the dragQuestion to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the dragQuestion, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/drag-questions/{id}")
    public ResponseEntity<DragQuestion> getDragQuestion(@PathVariable Long id) {
        log.debug("REST request to get DragQuestion : {}", id);
        Optional<DragQuestion> dragQuestion = dragQuestionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(dragQuestion);
    }

    /**
     * {@code DELETE  /drag-questions/:id} : delete the "id" dragQuestion.
     *
     * @param id the id of the dragQuestion to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/drag-questions/{id}")
    public ResponseEntity<Void> deleteDragQuestion(@PathVariable Long id) {
        log.debug("REST request to delete DragQuestion : {}", id);
        dragQuestionService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
