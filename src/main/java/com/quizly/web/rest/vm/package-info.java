/**
 * View Models used by Spring MVC REST controllers.
 */
package com.quizly.web.rest.vm;
