package com.quizly.web.rest;

import com.quizly.domain.OpenQuestion;
import com.quizly.repository.OpenQuestionRepository;
import com.quizly.service.OpenQuestionService;
import com.quizly.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.quizly.domain.OpenQuestion}.
 */
@RestController
@RequestMapping("/api")
public class OpenQuestionResource {

    private final Logger log = LoggerFactory.getLogger(OpenQuestionResource.class);

    private static final String ENTITY_NAME = "openQuestion";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OpenQuestionService openQuestionService;

    private final OpenQuestionRepository openQuestionRepository;

    public OpenQuestionResource(OpenQuestionService openQuestionService, OpenQuestionRepository openQuestionRepository) {
        this.openQuestionService = openQuestionService;
        this.openQuestionRepository = openQuestionRepository;
    }

    /**
     * {@code POST  /open-questions} : Create a new openQuestion.
     *
     * @param openQuestion the openQuestion to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new openQuestion, or with status {@code 400 (Bad Request)} if the openQuestion has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/open-questions")
    public ResponseEntity<OpenQuestion> createOpenQuestion(@Valid @RequestBody OpenQuestion openQuestion) throws URISyntaxException {
        log.debug("REST request to save OpenQuestion : {}", openQuestion);
        if (openQuestion.getId() != null) {
            throw new BadRequestAlertException("A new openQuestion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OpenQuestion result = openQuestionService.save(openQuestion);
        return ResponseEntity
            .created(new URI("/api/open-questions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /open-questions/:id} : Updates an existing openQuestion.
     *
     * @param id the id of the openQuestion to save.
     * @param openQuestion the openQuestion to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated openQuestion,
     * or with status {@code 400 (Bad Request)} if the openQuestion is not valid,
     * or with status {@code 500 (Internal Server Error)} if the openQuestion couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/open-questions/{id}")
    public ResponseEntity<OpenQuestion> updateOpenQuestion(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody OpenQuestion openQuestion
    ) throws URISyntaxException {
        log.debug("REST request to update OpenQuestion : {}, {}", id, openQuestion);
        if (openQuestion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, openQuestion.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!openQuestionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        OpenQuestion result = openQuestionService.save(openQuestion);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, openQuestion.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /open-questions/:id} : Partial updates given fields of an existing openQuestion, field will ignore if it is null
     *
     * @param id the id of the openQuestion to save.
     * @param openQuestion the openQuestion to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated openQuestion,
     * or with status {@code 400 (Bad Request)} if the openQuestion is not valid,
     * or with status {@code 404 (Not Found)} if the openQuestion is not found,
     * or with status {@code 500 (Internal Server Error)} if the openQuestion couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/open-questions/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<OpenQuestion> partialUpdateOpenQuestion(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody OpenQuestion openQuestion
    ) throws URISyntaxException {
        log.debug("REST request to partial update OpenQuestion partially : {}, {}", id, openQuestion);
        if (openQuestion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, openQuestion.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!openQuestionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<OpenQuestion> result = openQuestionService.partialUpdate(openQuestion);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, openQuestion.getId().toString())
        );
    }

    /**
     * {@code GET  /open-questions} : get all the openQuestions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of openQuestions in body.
     */
    @GetMapping("/open-questions")
    public List<OpenQuestion> getAllOpenQuestions() {
        log.debug("REST request to get all OpenQuestions");
        return openQuestionService.findAll();
    }

    /**
     * {@code GET  /open-questions/:id} : get the "id" openQuestion.
     *
     * @param id the id of the openQuestion to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the openQuestion, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/open-questions/{id}")
    public ResponseEntity<OpenQuestion> getOpenQuestion(@PathVariable Long id) {
        log.debug("REST request to get OpenQuestion : {}", id);
        Optional<OpenQuestion> openQuestion = openQuestionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(openQuestion);
    }

    /**
     * {@code DELETE  /open-questions/:id} : delete the "id" openQuestion.
     *
     * @param id the id of the openQuestion to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/open-questions/{id}")
    public ResponseEntity<Void> deleteOpenQuestion(@PathVariable Long id) {
        log.debug("REST request to delete OpenQuestion : {}", id);
        openQuestionService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
