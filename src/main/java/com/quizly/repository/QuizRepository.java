package com.quizly.repository;

import com.quizly.domain.Quiz;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Quiz entity.
 */
@SuppressWarnings("unused")
@Repository
public interface QuizRepository extends JpaRepository<Quiz, Long> {
    @Query("select quiz from Quiz quiz where quiz.user.login = ?#{principal.username}")
    List<Quiz> findByUserIsCurrentUser();
}
