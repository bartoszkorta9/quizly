package com.quizly.repository;

import com.quizly.domain.OpenQuestion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the OpenQuestion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OpenQuestionRepository extends JpaRepository<OpenQuestion, Long> {}
