package com.quizly.repository;

import com.quizly.domain.Score;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Score entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ScoreRepository extends JpaRepository<Score, Long> {
    @Query("select score from Score score where score.user.login = ?#{principal.username}")
    List<Score> findByUserIsCurrentUser();
}
