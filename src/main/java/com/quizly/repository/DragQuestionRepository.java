package com.quizly.repository;

import com.quizly.domain.DragQuestion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the DragQuestion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DragQuestionRepository extends JpaRepository<DragQuestion, Long> {}
