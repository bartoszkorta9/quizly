package com.quizly.repository;

import com.quizly.domain.BasicQuestion;
import com.quizly.domain.Question;
import com.quizly.domain.Quiz;
import java.util.List;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the BasicQuestion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BasicQuestionRepository extends JpaRepository<BasicQuestion, Long> {}
