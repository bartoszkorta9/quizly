package com.quizly.security.oauth2;

import com.quizly.security.jwt.TokenProvider;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class OAuth2LoginSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    private TokenProvider tokenProvider;
    private final String DEFAULT_REDIRECT_URI = "/api/users";
    private final Logger log = LoggerFactory.getLogger(OAuth2LoginSuccessHandler.class);
    private final CookieAuthRequestRepository cookieAuthRequestRepository;

    public OAuth2LoginSuccessHandler(TokenProvider tokenProvider, CookieAuthRequestRepository cookieAuthRequestRepository) {
        this.tokenProvider = tokenProvider;
        this.cookieAuthRequestRepository = cookieAuthRequestRepository;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
        throws IOException, ServletException {
        Object principal = authentication.getPrincipal();
        CustomOAuth2User loggedInUser = (CustomOAuth2User) principal;
        if (principal == null) throw new IllegalArgumentException("Null principal");
        String redirectUri = DEFAULT_REDIRECT_URI;
        Cookie cookie = cookieAuthRequestRepository.getCookie(request, "redirect_uri");
        if (cookie != null) {
            redirectUri = cookie.getValue();
        }
        log.debug("Redirect Uri is: " + redirectUri);
        if (response.isCommitted()) {
            logger.error("Response has already been commited. Unable to redirect to:" + redirectUri);
            return;
        }
        String token = tokenProvider.createToken(loggedInUser.getName(), loggedInUser.getAuthorities(), true);
        log.debug("Token is:" + token);
        String targetUri = UriComponentsBuilder.fromUriString(redirectUri).queryParam("token", token).build().toUriString();
        super.clearAuthenticationAttributes(request);
        cookieAuthRequestRepository.removeAuthorizationRequestCookies(request, response);
        getRedirectStrategy().sendRedirect(request, response, targetUri);
    }
}
