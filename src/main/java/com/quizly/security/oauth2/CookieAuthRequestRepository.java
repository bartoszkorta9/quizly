package com.quizly.security.oauth2;

import com.sun.xml.bind.v2.schemagen.xmlschema.Any;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Base64;
import java.util.Optional;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.client.web.AuthorizationRequestRepository;
import org.springframework.security.oauth2.core.endpoint.OAuth2AuthorizationRequest;
import org.springframework.stereotype.Component;

@Component
public class CookieAuthRequestRepository implements AuthorizationRequestRepository<OAuth2AuthorizationRequest> {

    private String OAUTH2_REQUEST_COOKIE_NAME = "oauth2_auth_request";
    private String REDIRECT_URI_PARAM_COOKIE_NAME = "redirect_uri";
    private int cookieExpireSeconds = 180;
    private final Logger log = LoggerFactory.getLogger(CookieAuthRequestRepository.class);

    @Override
    public OAuth2AuthorizationRequest loadAuthorizationRequest(HttpServletRequest httpServletRequest) {
        Cookie cookie = getCookie(httpServletRequest, OAUTH2_REQUEST_COOKIE_NAME);
        if (cookie == null) {
            log.debug("Cookie not found, for name:" + OAUTH2_REQUEST_COOKIE_NAME);
            return null;
        }
        OAuth2AuthorizationRequest oauth2request = deserialize(cookie, OAuth2AuthorizationRequest.class);
        return oauth2request;
    }

    @Override
    public void saveAuthorizationRequest(
        OAuth2AuthorizationRequest oAuth2AuthorizationRequest,
        HttpServletRequest httpServletRequest,
        HttpServletResponse httpServletResponse
    ) {
        if (oAuth2AuthorizationRequest == null) {
            log.debug("Deleting oauth2 cookies");
            deleteCookie(httpServletRequest, httpServletResponse, OAUTH2_REQUEST_COOKIE_NAME);
            deleteCookie(httpServletRequest, httpServletResponse, REDIRECT_URI_PARAM_COOKIE_NAME);
            return;
        }
        log.debug("Storing oauth2 cookie:" + OAUTH2_REQUEST_COOKIE_NAME);
        addCookie(httpServletResponse, OAUTH2_REQUEST_COOKIE_NAME, serialize(oAuth2AuthorizationRequest), cookieExpireSeconds);
        String redirectUriAfterLogin = httpServletRequest.getParameter(REDIRECT_URI_PARAM_COOKIE_NAME);
        if (redirectUriAfterLogin != null && !redirectUriAfterLogin.isBlank()) {
            log.debug("Storing redirect cookie" + OAUTH2_REQUEST_COOKIE_NAME + ", redirect:" + redirectUriAfterLogin);
            addCookie(httpServletResponse, REDIRECT_URI_PARAM_COOKIE_NAME, redirectUriAfterLogin, cookieExpireSeconds);
        }
    }

    public void addCookie(HttpServletResponse response, String name, String value, int maxAge) {
        Cookie cookie = new Cookie(name, value);
        cookie.setPath("/");
        cookie.setHttpOnly(true);
        cookie.setMaxAge(maxAge);
        response.addCookie(cookie);
        log.debug("Cookie added to the response");
    }

    private String serialize(OAuth2AuthorizationRequest obj) {
        return Base64.getUrlEncoder().encodeToString(SerializationUtils.serialize(obj));
    }

    private <T> T deserialize(Cookie cookie, Class<T> cls) {
        return cls.cast(SerializationUtils.deserialize(Base64.getUrlDecoder().decode(cookie.getValue())));
    }

    @Override
    public OAuth2AuthorizationRequest removeAuthorizationRequest(HttpServletRequest request) {
        return loadAuthorizationRequest(request);
    }

    public void removeAuthorizationRequestCookies(HttpServletRequest request, HttpServletResponse response) {
        deleteCookie(request, response, OAUTH2_REQUEST_COOKIE_NAME);
        deleteCookie(request, response, REDIRECT_URI_PARAM_COOKIE_NAME);
    }

    public void deleteCookie(HttpServletRequest request, HttpServletResponse response, String name) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (int i = 0; i < cookies.length; i++) {
                if (cookies[i].getName() == name) {
                    cookies[i].setValue("");
                    cookies[i].setPath("/");
                    cookies[i].setMaxAge(0);
                    response.addCookie(cookies[i]);
                }
            }
        }
    }

    public Cookie getCookie(HttpServletRequest request, String name) {
        Cookie[] cookies = request.getCookies();
        for (int i = 0; i < cookies.length; i++) {
            if (cookies[i].getName().equals(name)) {
                return cookies[i];
            }
        }
        return null;
    }
}
