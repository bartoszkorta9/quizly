package com.quizly.security.oauth2;

import com.quizly.domain.Authority;
import com.quizly.domain.User;
import com.quizly.repository.AuthorityRepository;
import com.quizly.repository.UserRepository;
import com.quizly.security.AuthoritiesConstants;
import com.quizly.web.rest.UserResource;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

@Service
public class CustomOAuth2UserService extends DefaultOAuth2UserService {

    private UserRepository userRepository;
    private AuthorityRepository authorityRepository;

    public CustomOAuth2UserService(UserRepository userRepository, AuthorityRepository authorityRepository) {
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
    }

    private final Logger log = LoggerFactory.getLogger(CustomOAuth2UserService.class);

    @Override
    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
        OAuth2User user = super.loadUser(userRequest);
        String registrationId = userRequest.getClientRegistration().getRegistrationId();
        log.debug("Loading user authorized via:" + registrationId);
        String email = user.getAttribute("email");
        if (email.isBlank()) throw new IllegalArgumentException("Missing email, can't look up in database" + user);

        Optional<User> dbUser = userRepository.findOneByEmailIgnoreCase(email);
        if (dbUser.isPresent()) {
            User updatedUser = dbUser.get();
            updatedUser.setImageUrl(user.getAttribute("picture"));
            String firstName = user.getAttribute("given_name");
            String lastName = user.getAttribute("family_name");
            updatedUser.setLastName(lastName);
            updatedUser.setFirstName(firstName);
            userRepository.save(updatedUser);
            log.debug("User was updated successfully.");
        } else {
            User newUser = new User();
            newUser.setEmail(email);
            newUser.setImageUrl(user.getAttribute("picture"));
            String firstName = user.getAttribute("given_name");
            String lastName = user.getAttribute("family_name");
            newUser.setExp(0);
            newUser.setLevel(0);
            newUser.setLastName(lastName);
            newUser.setFirstName(firstName);
            String newLogin = "google" + user.getAttribute("sub");
            newUser.setLogin(newLogin);
            newUser.setActivated(true);
            Set<Authority> authorities = new HashSet<>();
            authorityRepository.findById(AuthoritiesConstants.USER).ifPresent(authorities::add);
            newUser.setAuthorities(authorities);
            userRepository.save(newUser);
            log.debug("User was created successfully.");
        }
        return new CustomOAuth2User(user);
    }
}
