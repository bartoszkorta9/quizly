package com.quizly.service;

import com.quizly.domain.Score;
import com.quizly.repository.ScoreRepository;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Score}.
 */
@Service
@Transactional
public class ScoreService {

    private final Logger log = LoggerFactory.getLogger(ScoreService.class);

    private final ScoreRepository scoreRepository;

    public ScoreService(ScoreRepository scoreRepository) {
        this.scoreRepository = scoreRepository;
    }

    /**
     * Save a score.
     *
     * @param score the entity to save.
     * @return the persisted entity.
     */
    public Score save(Score score) {
        log.debug("Request to save Score : {}", score);
        return scoreRepository.save(score);
    }

    /**
     * Partially update a score.
     *
     * @param score the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Score> partialUpdate(Score score) {
        log.debug("Request to partially update Score : {}", score);

        return scoreRepository
            .findById(score.getId())
            .map(
                existingScore -> {
                    if (score.getPlace() != null) {
                        existingScore.setPlace(score.getPlace());
                    }
                    if (score.getDate() != null) {
                        existingScore.setDate(score.getDate());
                    }

                    return existingScore;
                }
            )
            .map(scoreRepository::save);
    }

    /**
     * Get all the scores.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Score> findAll() {
        log.debug("Request to get all Scores");
        return scoreRepository.findAll();
    }

    /**
     * Get one score by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Score> findOne(Long id) {
        log.debug("Request to get Score : {}", id);
        return scoreRepository.findById(id);
    }

    /**
     * Delete the score by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Score : {}", id);
        scoreRepository.deleteById(id);
    }
}
