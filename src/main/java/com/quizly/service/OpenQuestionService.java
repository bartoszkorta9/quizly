package com.quizly.service;

import com.quizly.domain.OpenQuestion;
import com.quizly.repository.OpenQuestionRepository;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link OpenQuestion}.
 */
@Service
@Transactional
public class OpenQuestionService {

    private final Logger log = LoggerFactory.getLogger(OpenQuestionService.class);

    private final OpenQuestionRepository openQuestionRepository;

    public OpenQuestionService(OpenQuestionRepository openQuestionRepository) {
        this.openQuestionRepository = openQuestionRepository;
    }

    /**
     * Save a openQuestion.
     *
     * @param openQuestion the entity to save.
     * @return the persisted entity.
     */
    public OpenQuestion save(OpenQuestion openQuestion) {
        log.debug("Request to save OpenQuestion : {}", openQuestion);
        return openQuestionRepository.save(openQuestion);
    }

    /**
     * Partially update a openQuestion.
     *
     * @param openQuestion the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<OpenQuestion> partialUpdate(OpenQuestion openQuestion) {
        log.debug("Request to partially update OpenQuestion : {}", openQuestion);

        return openQuestionRepository
            .findById(openQuestion.getId())
            .map(
                existingOpenQuestion -> {
                    if (openQuestion.getCorrect() != null) {
                        existingOpenQuestion.setCorrect(openQuestion.getCorrect());
                    }

                    return existingOpenQuestion;
                }
            )
            .map(openQuestionRepository::save);
    }

    /**
     * Get all the openQuestions.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<OpenQuestion> findAll() {
        log.debug("Request to get all OpenQuestions");
        return openQuestionRepository.findAll();
    }

    /**
     * Get one openQuestion by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OpenQuestion> findOne(Long id) {
        log.debug("Request to get OpenQuestion : {}", id);
        return openQuestionRepository.findById(id);
    }

    /**
     * Delete the openQuestion by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete OpenQuestion : {}", id);
        openQuestionRepository.deleteById(id);
    }
}
