package com.quizly.service;

import com.quizly.domain.Quiz;
import com.quizly.repository.QuizRepository;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Quiz}.
 */
@Service
@Transactional
public class QuizService {

    private final Logger log = LoggerFactory.getLogger(QuizService.class);

    private final QuizRepository quizRepository;

    public QuizService(QuizRepository quizRepository) {
        this.quizRepository = quizRepository;
    }

    /**
     * Save a quiz.
     *
     * @param quiz the entity to save.
     * @return the persisted entity.
     */
    public Quiz save(Quiz quiz) {
        log.debug("Request to save Quiz : {}", quiz);
        return quizRepository.save(quiz);
    }

    /**
     * Partially update a quiz.
     *
     * @param quiz the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Quiz> partialUpdate(Quiz quiz) {
        log.debug("Request to partially update Quiz : {}", quiz);

        return quizRepository
            .findById(quiz.getId())
            .map(
                existingQuiz -> {
                    if (quiz.getTitle() != null) {
                        existingQuiz.setTitle(quiz.getTitle());
                    }
                    if (quiz.getDescription() != null) {
                        existingQuiz.setDescription(quiz.getDescription());
                    }
                    if (quiz.getQuestions() != null) {
                        existingQuiz.setQuestions(quiz.getQuestions());
                    }

                    return existingQuiz;
                }
            )
            .map(quizRepository::save);
    }

    /**
     * Get all the quizzes.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Quiz> findAll() {
        log.debug("Request to get all Quizzes");
        return quizRepository.findAll();
    }

    /**
     * Get one quiz by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Quiz> findOne(Long id) {
        log.debug("Request to get Quiz : {}", id);
        return quizRepository.findById(id);
    }

    /**
     * Delete the quiz by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Quiz : {}", id);
        quizRepository.deleteById(id);
    }

    public List<Quiz> findByUserIsCurrentUser() {
        return quizRepository.findByUserIsCurrentUser();
    }
}
