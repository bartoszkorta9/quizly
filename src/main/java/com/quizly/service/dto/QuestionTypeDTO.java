package com.quizly.service.dto;

public class QuestionTypeDTO {

    private Object answers;
    private String type;
    private int number;
    private String question;

    public QuestionTypeDTO() {}

    public QuestionTypeDTO(Object answers, String type, int number, String question) {
        this.answers = answers;
        this.type = type;
        this.number = number;
        this.question = question;
    }

    public Object getAnswers() {
        return answers;
    }

    public void setAnswers(Object answers) {
        this.answers = answers;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
