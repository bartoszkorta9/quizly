package com.quizly.service.dto;

public class OpenQuestionDTO {

    private String question;
    private Integer time;
    private String imageUrl;
    private Integer number;
    private Long quizId;
    private String correct;

    public OpenQuestionDTO() {}

    public OpenQuestionDTO(OpenQuestionDTO openQuestion) {
        this.correct = openQuestion.getCorrect();
        this.imageUrl = openQuestion.getImageUrl();
        this.number = openQuestion.getNumber();
        this.question = openQuestion.getQuestion();
        this.quizId = openQuestion.getQuizId();
        this.time = openQuestion.getTime();
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }

    public String getCorrect() {
        return correct;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }
}
