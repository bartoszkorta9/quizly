package com.quizly.service.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.quizly.domain.Quiz;
import com.quizly.domain.User;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

public class NewScoreDTO {

    private Integer place;
    private Long quizId;

    public NewScoreDTO() {}

    public NewScoreDTO(Integer place, Long quizId) {
        this.place = place;
        this.quizId = quizId;
    }

    public Integer getPlace() {
        return place;
    }

    public void setPlace(Integer place) {
        this.place = place;
    }

    public Long getQuizId() {
        return quizId;
    }

    public void setQuizId(Long quizId) {
        this.quizId = quizId;
    }
}
