package com.quizly.service.dto;

public class ListQuestionDTO {

    private String type;
    private String answerOne;
    private String answerTwo;
    private String answerThree;
    private String answerFour;
    private int correct;
    private String question;

    public ListQuestionDTO() {}

    public ListQuestionDTO(
        String type,
        String answerOne,
        String answerTwo,
        String answerThree,
        String answerFour,
        int correct,
        String question
    ) {
        this.type = type;
        this.answerOne = answerOne;
        this.answerTwo = answerTwo;
        this.answerThree = answerThree;
        this.answerFour = answerFour;
        this.correct = correct;
        this.question = question;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAnswerOne() {
        return answerOne;
    }

    public void setAnswerOne(String answerOne) {
        this.answerOne = answerOne;
    }

    public String getAnswerTwo() {
        return answerTwo;
    }

    public void setAnswerTwo(String answerTwo) {
        this.answerTwo = answerTwo;
    }

    public String getAnswerThree() {
        return answerThree;
    }

    public void setAnswerThree(String answerThree) {
        this.answerThree = answerThree;
    }

    public String getAnswerFour() {
        return answerFour;
    }

    public void setAnswerFour(String answerFour) {
        this.answerFour = answerFour;
    }

    public int getCorrect() {
        return correct;
    }

    public void setCorrect(int correct) {
        this.correct = correct;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswerByNumber(int number) {
        if (number == 1) return this.answerOne; else if (number == 2) return this.answerTwo; else if (
            number == 3
        ) return this.answerThree; else return this.answerFour;
    }
}
