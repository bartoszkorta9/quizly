package com.quizly.service;

import com.quizly.domain.DragQuestion;
import com.quizly.repository.DragQuestionRepository;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link DragQuestion}.
 */
@Service
@Transactional
public class DragQuestionService {

    private final Logger log = LoggerFactory.getLogger(DragQuestionService.class);

    private final DragQuestionRepository dragQuestionRepository;

    public DragQuestionService(DragQuestionRepository dragQuestionRepository) {
        this.dragQuestionRepository = dragQuestionRepository;
    }

    /**
     * Save a dragQuestion.
     *
     * @param dragQuestion the entity to save.
     * @return the persisted entity.
     */
    public DragQuestion save(DragQuestion dragQuestion) {
        log.debug("Request to save DragQuestion : {}", dragQuestion);
        return dragQuestionRepository.save(dragQuestion);
    }

    /**
     * Partially update a dragQuestion.
     *
     * @param dragQuestion the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<DragQuestion> partialUpdate(DragQuestion dragQuestion) {
        log.debug("Request to partially update DragQuestion : {}", dragQuestion);

        return dragQuestionRepository
            .findById(dragQuestion.getId())
            .map(
                existingDragQuestion -> {
                    if (dragQuestion.getAnswerOne() != null) {
                        existingDragQuestion.setAnswerOne(dragQuestion.getAnswerOne());
                    }
                    if (dragQuestion.getAnswerTwo() != null) {
                        existingDragQuestion.setAnswerTwo(dragQuestion.getAnswerTwo());
                    }
                    if (dragQuestion.getAnswerThree() != null) {
                        existingDragQuestion.setAnswerThree(dragQuestion.getAnswerThree());
                    }
                    if (dragQuestion.getAnswerFour() != null) {
                        existingDragQuestion.setAnswerFour(dragQuestion.getAnswerFour());
                    }
                    if (dragQuestion.getCorrectOrder() != null) {
                        existingDragQuestion.setCorrectOrder(dragQuestion.getCorrectOrder());
                    }

                    return existingDragQuestion;
                }
            )
            .map(dragQuestionRepository::save);
    }

    /**
     * Get all the dragQuestions.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<DragQuestion> findAll() {
        log.debug("Request to get all DragQuestions");
        return dragQuestionRepository.findAll();
    }

    /**
     * Get one dragQuestion by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<DragQuestion> findOne(Long id) {
        log.debug("Request to get DragQuestion : {}", id);
        return dragQuestionRepository.findById(id);
    }

    /**
     * Delete the dragQuestion by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete DragQuestion : {}", id);
        dragQuestionRepository.deleteById(id);
    }
}
