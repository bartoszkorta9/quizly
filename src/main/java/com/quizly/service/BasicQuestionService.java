package com.quizly.service;

import com.quizly.domain.BasicQuestion;
import com.quizly.repository.BasicQuestionRepository;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link BasicQuestion}.
 */
@Service
@Transactional
public class BasicQuestionService {

    private final Logger log = LoggerFactory.getLogger(BasicQuestionService.class);

    private final BasicQuestionRepository basicQuestionRepository;

    public BasicQuestionService(BasicQuestionRepository basicQuestionRepository) {
        this.basicQuestionRepository = basicQuestionRepository;
    }

    /**
     * Save a basicQuestion.
     *
     * @param basicQuestion the entity to save.
     * @return the persisted entity.
     */
    public BasicQuestion save(BasicQuestion basicQuestion) {
        log.debug("Request to save BasicQuestion : {}", basicQuestion);
        return basicQuestionRepository.save(basicQuestion);
    }

    /**
     * Partially update a basicQuestion.
     *
     * @param basicQuestion the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<BasicQuestion> partialUpdate(BasicQuestion basicQuestion) {
        log.debug("Request to partially update BasicQuestion : {}", basicQuestion);

        return basicQuestionRepository
            .findById(basicQuestion.getId())
            .map(
                existingBasicQuestion -> {
                    if (basicQuestion.getAnswerOne() != null) {
                        existingBasicQuestion.setAnswerOne(basicQuestion.getAnswerOne());
                    }
                    if (basicQuestion.getAnswerTwo() != null) {
                        existingBasicQuestion.setAnswerTwo(basicQuestion.getAnswerTwo());
                    }
                    if (basicQuestion.getAnswerThree() != null) {
                        existingBasicQuestion.setAnswerThree(basicQuestion.getAnswerThree());
                    }
                    if (basicQuestion.getAnswerFour() != null) {
                        existingBasicQuestion.setAnswerFour(basicQuestion.getAnswerFour());
                    }
                    if (basicQuestion.getCorrect() != null) {
                        existingBasicQuestion.setCorrect(basicQuestion.getCorrect());
                    }

                    return existingBasicQuestion;
                }
            )
            .map(basicQuestionRepository::save);
    }

    /**
     * Get all the basicQuestions.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<BasicQuestion> findAll() {
        log.debug("Request to get all BasicQuestions");
        return basicQuestionRepository.findAll();
    }

    /**
     * Get one basicQuestion by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<BasicQuestion> findOne(Long id) {
        log.debug("Request to get BasicQuestion : {}", id);
        return basicQuestionRepository.findById(id);
    }

    /**
     * Delete the basicQuestion by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete BasicQuestion : {}", id);
        basicQuestionRepository.deleteById(id);
    }
}
