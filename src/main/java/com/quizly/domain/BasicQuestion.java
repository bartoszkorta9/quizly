package com.quizly.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A BasicQuestion.
 */
@Entity
@Table(name = "basic_question")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@PrimaryKeyJoinColumn(name = "question_id")
public class BasicQuestion extends Question implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "question_id", nullable = false, unique = true)
    private Long id;

    @NotNull
    @Column(name = "answer_one", nullable = false)
    private String answerOne;

    @NotNull
    @Column(name = "answer_two", nullable = false)
    private String answerTwo;

    @Column(name = "answer_three")
    private String answerThree;

    @Column(name = "answer_four")
    private String answerFour;

    @NotNull
    @Column(name = "correct", nullable = false)
    private Integer correct;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BasicQuestion id(Long id) {
        this.id = id;
        return this;
    }

    public String getAnswerOne() {
        return this.answerOne;
    }

    public BasicQuestion answerOne(String answerOne) {
        this.answerOne = answerOne;
        return this;
    }

    public void setAnswerOne(String answerOne) {
        this.answerOne = answerOne;
    }

    public String getAnswerTwo() {
        return this.answerTwo;
    }

    public BasicQuestion answerTwo(String answerTwo) {
        this.answerTwo = answerTwo;
        return this;
    }

    public void setAnswerTwo(String answerTwo) {
        this.answerTwo = answerTwo;
    }

    public String getAnswerThree() {
        return this.answerThree;
    }

    public BasicQuestion answerThree(String answerThree) {
        this.answerThree = answerThree;
        return this;
    }

    public void setAnswerThree(String answerThree) {
        this.answerThree = answerThree;
    }

    public String getAnswerFour() {
        return this.answerFour;
    }

    public BasicQuestion answerFour(String answerFour) {
        this.answerFour = answerFour;
        return this;
    }

    public void setAnswerFour(String answerFour) {
        this.answerFour = answerFour;
    }

    public Integer getCorrect() {
        return this.correct;
    }

    public BasicQuestion correct(Integer correct) {
        this.correct = correct;
        return this;
    }

    public void setCorrect(Integer correct) {
        this.correct = correct;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BasicQuestion)) {
            return false;
        }
        return id != null && id.equals(((BasicQuestion) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BasicQuestion{" +
            "id=" + getId() +
            ", answerOne='" + getAnswerOne() + "'" +
            ", answerTwo='" + getAnswerTwo() + "'" +
            ", answerThree='" + getAnswerThree() + "'" +
            ", answerFour='" + getAnswerFour() + "'" +
            ", correct=" + getCorrect() +
            "}";
    }
}
