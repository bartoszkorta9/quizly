package com.quizly.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A DragQuestion.
 */
@Entity
@Table(name = "drag_question")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@PrimaryKeyJoinColumn(name = "question_id")
public class DragQuestion extends Question implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "question_id", nullable = false, unique = true)
    private Long id;

    @NotNull
    @Column(name = "answer_one", nullable = false)
    private String answerOne;

    @NotNull
    @Column(name = "answer_two", nullable = false)
    private String answerTwo;

    @Column(name = "answer_three")
    private String answerThree;

    @Column(name = "answer_four")
    private String answerFour;

    @NotNull
    @Column(name = "correct_order", nullable = false)
    private String correctOrder;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DragQuestion id(Long id) {
        this.id = id;
        return this;
    }

    public String getAnswerOne() {
        return this.answerOne;
    }

    public DragQuestion answerOne(String answerOne) {
        this.answerOne = answerOne;
        return this;
    }

    public void setAnswerOne(String answerOne) {
        this.answerOne = answerOne;
    }

    public String getAnswerTwo() {
        return this.answerTwo;
    }

    public DragQuestion answerTwo(String answerTwo) {
        this.answerTwo = answerTwo;
        return this;
    }

    public void setAnswerTwo(String answerTwo) {
        this.answerTwo = answerTwo;
    }

    public String getAnswerThree() {
        return this.answerThree;
    }

    public DragQuestion answerThree(String answerThree) {
        this.answerThree = answerThree;
        return this;
    }

    public void setAnswerThree(String answerThree) {
        this.answerThree = answerThree;
    }

    public String getAnswerFour() {
        return this.answerFour;
    }

    public DragQuestion answerFour(String answerFour) {
        this.answerFour = answerFour;
        return this;
    }

    public void setAnswerFour(String answerFour) {
        this.answerFour = answerFour;
    }

    public String getCorrectOrder() {
        return this.correctOrder;
    }

    public DragQuestion correctOrder(String correctOrder) {
        this.correctOrder = correctOrder;
        return this;
    }

    public void setCorrectOrder(String correctOrder) {
        this.correctOrder = correctOrder;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof DragQuestion)) {
            return false;
        }
        return id != null && id.equals(((DragQuestion) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "DragQuestion{" +
            "id=" + getId() +
            ", answerOne='" + getAnswerOne() + "'" +
            ", answerTwo='" + getAnswerTwo() + "'" +
            ", answerThree='" + getAnswerThree() + "'" +
            ", answerFour='" + getAnswerFour() + "'" +
            ", correctOrder='" + getCorrectOrder() + "'" +
            "}";
    }
}
