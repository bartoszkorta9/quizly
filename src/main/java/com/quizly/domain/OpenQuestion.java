package com.quizly.domain;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A OpenQuestion.
 */
@Entity
@Table(name = "open_question")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@PrimaryKeyJoinColumn(name = "question_id")
public class OpenQuestion extends Question implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "question_id", nullable = false, unique = true)
    private Long id;

    @NotNull
    @Column(name = "correct", nullable = false)
    private String correct;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OpenQuestion id(Long id) {
        this.id = id;
        return this;
    }

    public String getCorrect() {
        return this.correct;
    }

    public OpenQuestion correct(String correct) {
        this.correct = correct;
        return this;
    }

    public void setCorrect(String correct) {
        this.correct = correct;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OpenQuestion)) {
            return false;
        }
        return id != null && id.equals(((OpenQuestion) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OpenQuestion{" +
            "id=" + getId() +
            ", correct='" + getCorrect() + "'" +
            "}";
    }
}
